<body>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">                    
            <?php $error = $this->session->flashdata("error"); ?>
					<div class="alert alert-<?php echo $error ? 'warning' : 'info' ?> alert-dismissible" role="alert">
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  <?php echo $error ? $error : 'Ingresa tus datos' ?>
					</div>
					<div class="form-group ">
                    <div class="col-xs-12">
                        <h3>Gana Municipal</h3>
                        <p class="text-muted">Ingresa aquí para conocer los proyectos</p>
                    </div>
           			</div>
					<?php echo form_open(); ?>  
        			<?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>              
					<div class="form-group <?php echo $error ? 'has-error' : '' ?>">
					<div class="input-group col-xs-12">
						<input type="text" name="username" value="<?php echo set_value("username") ?>" id="username" class="form-control" placeholder="Cédula">
					</div>  
  					<?php echo $error; ?>
					</div>
        			<?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
					<div class="form-group <?php echo $error ? 'has-error' : '' ?>">
					<div class="input-group col-xs-12">
						<input type="password" name="password" id="password" class="form-control" placeholder="Contraseña">
					</div> 
  					<?php echo $error; ?>
					</div>
					<input type="submit" value="Ingresar" class="btn btn-success btn-lg btn-block  waves-light">
					<div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>¿No puedes ingresar? <a href="<?php echo site_url('auth/formularioRegistrarUsuario') ?>" class="text-primary m-l-5"><b>¡Regístrate!</b></a></p>
                        </div>
                    </div>
					<?php echo form_close(); ?>
            </div>
            <footer class="footer text-center">
            </footer>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url()."assets/bower_components/jquery/dist/jquery.min.js"?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()."assets/bower_components/bootstrap/dist/js/bootstrap.min.js"?>"></script>