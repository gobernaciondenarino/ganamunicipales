<section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <?php  echo form_open('/auth/agregarUsuario', 'class="form-validate form-horizontal" id="formRegistrar"') ?>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Registro de Usuarios</h3>
                            <p class="text-muted">Diligencia el formulario</p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input required class="form-control"  minlength=5 placeholder="Identificación" name="username">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input required class="form-control" type="text"  placeholder="Nombre" name="nombre">
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input required class="form-control" type="password"  placeholder="Contraseña" name="password">
                        </div>
                    </div>
                    <div class="form-group ">
                    	<div class="col-xs-12">
                          <select class="form-control" name="municipio" form="formRegistrar" required="required">
								<option value="Mallama">Mallama</option>
								<option value="Belén">Belén</option>
								<option value="La Cruz">La Cruz</option>
								<option value="Ricaurte">Ricaurte</option>
								<option value="Sandoná">Sandoná</option>
								<option value="San Pedro de Cartago">San Pedro de Cartago</option>
								<option value="Providencia">Providencia</option>
								<option value="Imués">Imués</option>
								<option value="San Pablo">San Pablo</option>
								<option value="Contadero">Contadero</option>
								<option value="Guaitarilla">Guaitarilla</option>
								<option value="Arboleda">Arboleda</option>
								<option value="Consacá">Consacá</option>	
								<option value="La Llanada">La Llanada</option>
								<option value="Buesaco">Buesaco</option>
								<option value="Gualmatán">Gualmatán</option>
								<option value="Ancuya">Ancuya</option>
								<option value="Taminango">Taminango</option>
								<option value="El Tablón de Gómez">El Tablón de Gómez</option>
								<option value="El Rosario">El Rosario</option>
								<option value="Los Andes">Los Andes</option>
								<option value="El Peñol">El Peñol</option>
         							<option value="Cuaspud">Cuaspud</option>
								<option value="Puerres">Puerres</option>
								<option value="Albán">Albán</option>
 								<option value="San Bernardo">San Bernardo</option>
								<option value="Ospina">Ospina</option>
								<option value="Sapuyes">Sapuyes</option>
								<option value="Guachucal">Guachucal</option>
								<option value="Santacruz">Santacruz</option>
								<option value="Policarpa">Policarpa</option>
								<option value="San Lorenzo">San Lorenzo</option>
								<option value="Linares">Linares</option>
								<option value="Colón">Colón</option>
								<option value="Aldana">Aldana</option>
								<option value="Barbacoas">Barbacoas</option>
								<option value="Chachagüí">Chachagüí</option>
								<option value="Córdoba">Córdoba</option>
								<option value="Cumbal">Cumbal</option>
								<option value="Cumbitara">Cumbitara</option>
								<option value="El Charco">El Charco</option>
								<option value="El Tambo">El Tambo</option>
								<option value="Francisco Pizarro">Francisco Pizarro</option>
								<option value="Funes">Funes</option>
								<option value="Iles">Iles</option>
								<option value="Ipiales">Ipiales</option>
								<option value="La Florida">La Florida</option>
								<option value="La Tola">La Tola</option>
								<option value="La Unión">La Unión</option>
								<option value="Leiva">Leiva</option>
								<option value="Magüí Payán">Magüí Payán</option>
								<option value="Mosquera">Mosquera</option>
								<option value="Nariño">Nariño</option>
								<option value="Olaya Herrera">Olaya Herrera</option>
								<option value="Pasto">Pasto</option>
								<option value="Potosí">Potosí</option>
								<option value="Pupiales">Pupiales</option>
								<option value="Roberto Payán">Roberto Payán</option>
								<option value="Samaniego">Samaniego</option>
								<option value="Santa Bárbara">Santa Bárbara</option>
								<option value="Tangua">Tangua</option>
								<option value="Tumaco">Tumaco</option>
								<option value="Túquerres">Túquerres</option>
								<option value="Yacuanquer">Yacuanquer</option>
						  </select>
						</div>
	                </div>
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block waves-light" type="submit">Registrarme</button>
                        </div>
                    </div>
                    <div class="form-group m-t-30 m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>¿Ya tienes usuario? <a href="<?php echo site_url('auth') ?>" class="text-primary m-l-5"><b>Ingresar</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
