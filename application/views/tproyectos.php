<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3>Panel</h3>
                            <hr>
                            <div class="row">
                                <div class="col-lg-2 col-md-3  col-sm-4 col-xs-12 ">
                                    <div class="p-r-20">
                                        <div class="list-group mail-list m-t-20"> 
	                                        <a href="<?php echo site_url('usu/tproyectos') ?>" class="list-group-item active"> Todos los Proyectos</a> 
	                                        <a href="<?php echo site_url('usu/mproyectos') ?>" class="list-group-item "> Mis Proyectos</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">                                    
                                    <div class="message-center">
                                    	<?php if($registros): ?>          
						                    <?php foreach($registros->result() as $reg): ?>
						                    	<a class="unread">
		                                            <div class="user-img">
		                                            <?php 
		                                            	if ($reg->estado=="Abierto") {?>
		                                            		<button type="button" class="btn btn-success btn-circle"></button><?php
		                                            	}
		                                            	else{?>
		                                            		<button type="button" class="btn btn-danger btn-circle"></i> </button><?php
		                                            	}
		                                            ?>		                                             
		                                            </div>
		                                            <div class="mail-contnet">
		                                                <h5><?php echo $reg->nombre; ?></h5>
		                                                <span class="mail-desc"><?php echo $reg->descripcion; ?> - <?php echo $reg->municipio; ?></span> 
		                                                <span class="mail-desc"><?php echo money_format('%#10n', $reg->valor) ?></span> 
		                                                <span class="mail-desc">
								                        <?php  echo form_open('/usu/verProyectoUsu', 'class="form-horizontal" id="formEditar"') ?>
															<input type="hidden" name="id_proy" value="<?php echo $reg->id; ?>">
															<button type="submit" class="btn btn-outline btn-rounded btn-default">Ver</button>
														<?php echo form_close() ?>
														</span>
		                                            </div>		                                        	
		                                        </a>												
											<?php endforeach;?>
										<?php else:?>
										<p>No hay datos en la base de datos</p>
										<?php  endif; ?>
                                        <!--
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status online pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5>
                                                <span class="mail-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span> <span class="time">9:30 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status busy pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Sonu Nigam</h5>
                                                <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        -->
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>