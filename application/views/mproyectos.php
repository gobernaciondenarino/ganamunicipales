<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                                                
                        <?php $saldo=$saldousuario->row() ?>
                    	<?php $nump=$numproyectos->row() ?>
                    	<?php $totp=$totproyectos->row() ?>
                    	<?php $conta=0; ?>
                    	<?php
                    		if ($registros!=false) { ?>
                    		 	<?php foreach($registros->result() as $reg): ?>
			                    	<?php if ($saldo->saldo>=$reg->valor or $saldo->saldo<$reg->valor): ?>	
			                    		<?php 
											if ($reg->agregado==1 or $saldo->saldo<$reg->valor) { ?>
											<?php }
											else { ?>
											    <?php $conta+=1; ?>
											<?php }
										?>
			                    	<?php endif ?>					                    													
								<?php endforeach;?>
						
	                            <!-- <h2>Saldo Actual: <strong><?php //echo "$ ".number_format($saldo->saldo, 0, '', '.') ?></strong><br>-->
	                            <h2>Proyectos Disponibles: <strong><?php echo " ".$conta; ?></strong><br>
	                            <input hidden id="totalf" value="<?php echo $saldo->saldo; ?>" name="saldo"><br>
	                            <?php  echo form_open('/usu/usudone', 'class="form-horizontal" id="formEditar"') ?>
									<input type="hidden" name="id_usu" value="<?php echo $this->session->userdata('id') ?>">
									<h4><strong>* </strong>Solo cuando este seguro de su elección haga clic en votar</h4>
									<button type="submit" class="btn btn-info btn-lg" onclick="modal()">Votar</button>
																<?php echo form_close() ?>
	                             </div>
	                             <div class="white-box">
	                             <h3><strong>Listado de Proyectos</strong><br>
                                    	<?php if($registros): ?> 
                                    		<table class="table" id="example">
                                    		<thead>
						                      <tr>
						                          <th><i class="icon_pencil"></i> </th>
						                         <th><i class="icon_pencil"></i> Proyecto</th>
						                         <th><i class="icon_pencil"></i> Valor</th>
						                         <th><i class="icon_profile"></i> </th>
						                         <th><i class="icon_cogs"></i> </th>
						                      </tr>
						                      </thead>
						                      <tbody>
						                          
						                      <?php foreach($registros->result() as $reg): ?>
						                    	<?php if ($saldo->saldo>=$reg->valor or $saldo->saldo<$reg->valor): ?>	
						                    		<tr>
							                            <!--<div class="text-muted"><span class="m-r-10"><?php echo $reg->estado; ?></span></div>-->
						                            <td><img width="100" height="100" class="img-responsive" alt="user" src="<?php echo $reg->adjunto; ?>"></td>
							                            <td><h3 class="m-t-20 m-b-20"><?php echo $reg->nombre; ?></h3></td>
							                           	<td><p align=right><?php echo "$ ".number_format($reg->valor, 0, '', '.') ?></p></td>		
							                           	<td>
									                        <?php  echo form_open('/usu/agregarVoto', 'class="form-horizontal" id="formEditar"') ?>
																<input type="hidden" name="id_proy" value="<?php echo $reg->proyecto_id; ?>">
																<input type="hidden" name="id_user" value="<?php echo $saldo->id; ?>">
																<input type="hidden" name="monto" value="<?php echo $reg->valor; ?>">
																<?php 
																	if ($reg->agregado==1 or $saldo->saldo<$reg->valor) { ?>
																		<button disabled type="submit" class="btn btn-secondary btn"><span class="icon_creditcard"></span> Agregar</button>
																	<?php }
																	else { ?>
																		<button type="submit" class="btn btn-info btn"><span class="icon_creditcard"></span> Agregar</button>
																	<?php }
																?>
																
															<?php echo form_close() ?>
							                           	</td>	
							                           	<td>
									                        <?php  echo form_open('/usu/quitarVoto', 'class="form-horizontal" id="formEditar"') ?>
																<input type="hidden" name="id_proy" value="<?php echo $reg->proyecto_id; ?>">
																<input type="hidden" name="id_user" value="<?php echo $saldo->id; ?>">
																<input type="hidden" name="monto" value="<?php echo $reg->valor; ?>">
																<?php 
																	if ($reg->agregado==1) { ?>
																		<button type="submit" class="btn btn-danger btn"><span class="icon_creditcard"></span> Quitar</button>
																	<?php }
																	else { ?>
																		<button disabled type="submit" class="btn btn-secondary btn"><span class="icon_creditcard"></span> Quitar</button>
																	<?php }
																?>
															<?php echo form_close() ?>
							                           	</td>
											        </tr>
						                    	<?php endif ?>					                    													
											<?php endforeach;?>
											</tbody>
			                				</table>
										<?php else:?>
										<p>No hay datos en la base de datos</p>
										<?php  endif; ?>
										
                                        <!--
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status online pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5>
                                                <span class="mail-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span> <span class="time">9:30 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status busy pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Sonu Nigam</h5>
                                                <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        --><?php
                    		 	} 
                    				else{
                    			?>
                        			<a href="<?php echo site_url('auth/logout') ?>">¡Para realizar la votación debes iniciar nuevamente!</a>
                        		<?php
                        			}  
                        		?>
                            
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
