<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Gestión de Usuarios</h3>
                            <?php if($registros): ?>          
			                <table class="table table-striped table-advance table-hover" id="example">
			                	  <thead>
			                      <tr>
			                         <th><i class="icon_pencil"></i> Año</th>
			                         <th><i class="icon_pencil"></i> Municipio</th>
			                         <th><i class="icon_profile"></i> Eficacia</th>
			                         <th><i class="icon_tag"></i> Eficiencia</th>                         
			                         <th><i class="icon_mail_alt"></i> Requisitos Legales</th>
			                         <th><i class="icon_mail_alt"></i> Gestión Administrativa</th>
			                         <th><i class="icon_mobile"></i> Desempeño Integral</th>
			                         <th><i class="icon_mobile"></i> Calificación</th>
			                         <th><i class="icon_cogs"></i> Acción</th>
			                      </tr>
			                      </thead>
			                      <tbody>
			                      <?php foreach($registros->result() as $reg): ?>
									<tr>
										<td><p align=left><?php echo $reg->anio; ?></p></td>
										<td><p align=left><?php echo $reg->municipio; ?></p></td>
										<td><p align=left><?php echo number_format($reg->eficacia, 2); ?></p></td>
										<td><p align=left><?php echo number_format($reg->eficiencia, 2); ?></p></td>							
										<td><p align=left><?php echo number_format($reg->requisitos, 2); ?></p></td>
										<td><p align=left><?php echo number_format($reg->gestion, 2); ?></p></td>
										<td><p align=left><?php echo number_format($reg->integral, 2); ?></p></td>
										<td>
											<p align=left>
												<?php 
												if ($reg->etiqueta=="Crítico") {
													?><button type="submit" disabled class="btn btn-danger btn-sm">Crítico</button>
													<?php 
												}
												elseif($reg->etiqueta=="Bajo"){
													?><button type="submit" disabled class="btn btn-warning btn-sm">Bajo</button>
													<?php 
												}
												elseif($reg->etiqueta=="Medio"){
													?><button type="submit" disabled class="btn btn-default btn-sm">Medio</button>
													<?php 
												}
												elseif($reg->etiqueta=="Satisfactorio"){
													?><button type="submit" disabled class="btn btn-info btn-sm">Satisfactorio</button>
													<?php 
												}
												elseif($reg->etiqueta=="Sobresaliente"){
													?><button type="submit" disabled class="btn btn-success btn-sm">Sobresaliente</button>
													<?php 
												}
												else{
													?>-
													<?php 
												}
												?>
											</p>
										</td>
										<td>
				                        <div class="btn-group">
					                        <?php  echo form_open('/admin/formularioEditarMunicipio', 'class="form-horizontal" id="formEditar"') ?>
												<input type="hidden" name="id_muni" value="<?php echo $reg->id; ?>">
												<input type="hidden" name="id_anio" value="<?php echo $reg->anio; ?>">
												<button type="submit" class="btn btn-success btn-sm">Editar</button>
											<?php echo form_close() ?>
										</div>
			                         </td>
									</tr>
								  <?php endforeach;?>
								  </tbody>
			                </table>
							<?php else:?>
							<p>No hay datos en la base de datos</p>
							<?php  endif; ?>
                        </div>
                    </div>