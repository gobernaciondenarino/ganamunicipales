<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Gestión de Usuarios</h3>
                            <?php if($registros): ?>          
			                <table class="table table-striped table-advance table-hover" id="example">
			                	  <thead>
			                      <tr>
			                         <th><i class="icon_pencil"></i> Nombre</th>
			                         <th><i class="icon_profile"></i> Usuario</th>
			                         <th><i class="icon_tag"></i> Rol</th>                         
			                         <th><i class="icon_mail_alt"></i> Correo</th>
			                         <th><i class="icon_mail_alt"></i> Dirección</th>
			                         <th><i class="icon_mobile"></i> Municipio</th>
			                         <th><i class="icon_mobile"></i> Celular</th>
			                         <th><i class="icon_cursor_alt"></i> Estado</th>
			                         <th><i class="icon_cursor_alt"></i> Saldo</th>
			                         <th><i class="icon_cogs"></i> Acción</th>
			                      </tr>
			                      </thead>
			                      <tbody>
			                      <?php foreach($registros->result() as $reg): ?>
									<tr>
										<td><p align=left><?php echo $reg->nombre; ?></p></td>
										<td><p align=left><?php echo $reg->username; ?></p></td>
										<td><p align=left><?php echo $reg->rol; ?></p></td>							
										<td><p align=left><?php echo $reg->correo; ?></p></td>
										<td><p align=left><?php echo $reg->direccion; ?></p></td>
										<td><p align=left><?php echo $reg->municipio; ?></p></td>
										<td><p align=left><?php echo $reg->celular; ?></p></td>
										<td><p align=left><?php echo $reg->estado; ?></p></td>
										<td><p align=left><?php echo $reg->saldo; ?></p></td>
										<td>
				                        <div class="btn-group">
					                        <?php  echo form_open('/admin/formularioEditarUsuario', 'class="form-horizontal" id="formEditar"') ?>
												<input type="hidden" name="id_usu" value="<?php echo $reg->id; ?>">
												<button type="submit" class="btn btn-success btn-sm">Editar</button>
											<?php echo form_close() ?>
										</div>
			                         </td>
									</tr>
								  <?php endforeach;?>
								  </tbody>
			                </table>
							<?php else:?>
							<p>No hay datos en la base de datos</p>
							<?php  endif; ?>
                        </div>
                    </div>