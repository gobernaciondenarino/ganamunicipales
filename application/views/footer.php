<footer></footer>
 	<!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url()."assets/bower_components/jquery/dist/jquery.min.js"?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()."assets/bower_components/bootstrap/dist/js/bootstrap.min.js"?>"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url()."assets/bower_components/metisMenu/dist/metisMenu.min.js"?>"></script>
    <!--Nice scroll JavaScript -->
    <script src="<?php echo base_url()."assets/js/jquery.nicescroll.js"?>"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="<?php echo base_url()."assets/bower_components/chartist-js/dist/chartist.min.js"?>"></script>
    <script src="<?php echo base_url()."assets/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"?>"></script>
    <script src="<?php echo base_url()."assets/bower_components/jquery-sparkline/jquery.sparkline.min.js"?>"></script>
    <script src="<?php echo base_url()."assets/bower_components/jquery-sparkline/jquery.charts-sparkline.js"?>"></script>
    <script src="<?php echo base_url()."assets/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js"?>"></script>
    <script src="<?php echo base_url()."assets/bower_components/vectormap/jquery-jvectormap-world-mill-en.js"?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()."assets/js/waves.js"?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()."assets/js/myadmin.js"?>"></script>
    <script src="<?php echo base_url()."assets/js/dashboard1.js"?>"></script>

    <script src="<?php echo base_url()."assets/js/jasny-bootstrap.js"?>"></script>

    <!-- jQuery file upload -->
    <script src="<?php echo base_url()."assets/bower_components/dropify/dist/js/dropify.min.js"?>"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>

</body>

</html>
