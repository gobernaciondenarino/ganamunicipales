<?php if($registros): ?>  
	<?php foreach($registros->result() as $reg): ?>
		<?php
		if ($reg->municipio=="Mallama") {
			$mallama=$reg->etiqueta;
		}
		elseif ($reg->municipio=="Ricaurte") {
			$ricaurte=$reg->etiqueta;
		}
		?>
	<?php endforeach;?>
<?php else:?>
<p>No hay datos en la base de datos</p>
<?php  endif; ?>
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
						    <script type="text/javascript">
					        //---------------------------------------------------------------------------------
					        function enter(area) {
					            switch (area) {
					                case 1: swal("<?php echo "Calificación: ".$mallama; ?>", "Indicador de Desempeño Integral", "warning"); break;
					                case 2: swal("<?php echo "Calificación: ".$ricaurte; ?>", "Indicador de Desempeño Integral", "warning"); break;
					            }
					        }
					        //---------------------------------------------------------------------------------
					    </script>
					    <table border="0" width="750">
					        <tr>
					            <td align="left">
					                <b>Pie de Monte Costero</b>
					            </td>
					        </tr>
					    </table>
					    <hr />
					    <table border="0" width="750">
					        <tr>
					            <td align="center">
					                <img id="subregion_img" src="<?php echo base_url()."assets/images/subregiones/subregion4.png"?>" usemap="#test" />
					                <map id="test" name="test">
					                    <area id="sh1" shape="poly" alt="Mallama" title="Mallama" coords="216,292,230,306,268,280,268,266,296,236,288,212,282,194,290,182,304,186,360,136,362,118,398,142,398,152,440,214,460,224,460,250,464,262,474,324,446,334,456,348,412,392,400,370,376,382,366,410,350,428,302,426,264,414,242,382,242,346" href="#" onclick="enter(1)" />
					                    <area id="sh2" shape="poly" alt="Ricaurte" title="Ricaurte" coords="238,386,242,346,230,320,220,304,230,298,236,312,264,288,270,262,298,238,288,218,288,188,318,170,340,158,336,146,362,118,362,108,364,78,370,70,366,60,348,64,338,34,344,22,330,-2,332,16,320,28,264,34,244,28,194,74,202,96,200,160,212,176,172,192,164,188,140,196,142,210,150,210,138,248,8,276,34,302,34,316,10,338,44,360,74,384,86,418,74,424,114,438,156,436" href="#" onclick="enter(2)" />
					                </map>
					                <script>
					                    (function ($) {
					                        jQuery("#sh1").mouseenter(function () {
					                            jQuery("#municipality_title").text("Municipio: Mallama");
					                        });
					                        jQuery("#sh2").mouseenter(function () {
					                            jQuery("#municipality_title").text("Municipio: Ricaurte");
					                        });
					                    })(jQuery);
					                 </script>
					            </td>
					        </tr>
					    </table>
						    <script>
						        (function ($) {
						            jQuery("#subregion").dialog({
						                autoOpen: false,
						                height: 600,
						                width: 800,
						                modal: true
						            });
						        })(jQuery);
						    </script>
                        </div>
                    </div>