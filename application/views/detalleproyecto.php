<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">                                
                                <div class="col-md-12">
                                	<?php if($proyecto): ?>          
					                <?php foreach($proyecto->result() as $reg): ?>
                                    <div class="media m-b-30 p-t-20 b-t">
                                        <a class="pull-left" href="#"> <img class="media-object thumb-sm img-circle" src="../assets/images/users/pawandeep.jpg" alt=""> </a>
                                        <div class="media-body"> <span class="media-meta pull-right">
                                        <?php 
                                        	if ($reg->estado=="Abierto") {?>
                                        		<button type="submit" disabled class="btn btn-success btn-sm">Abierto</button><?php
                                        	}
                                        	else{?>
                                        		<button type="submit" disabled class="btn btn-danger btn-sm">Cerrado</button><?php
                                        	}
                                        ?>
                                        </span>
                                            <h4 class="text-danger m-0"><?php echo $reg->nombre; ?></h4><br>
                                            <h5 class="text-info m-0"><?php echo $reg->municipio; ?></h5>
                                            <small class="text-muted">Valor: <?php echo money_format('%#10n', $reg->valor) ?></small><br>
                                            <small class="text-muted">Autor:  <?php echo $reg->autor; ?></small> 
                                            <small class="text-muted">Vigencia:  <?php echo $reg->fechafin; ?> hasta <?php echo $reg->fechafin; ?></small> 
                                        </div>
                                    </div>                                    
                                    <p><?php echo $reg->descripcion; ?></p>
                                    <h5><i class="fa fa-paperclip m-r-10 m-b-10"></i> <?php echo $reg->adjunto; ?></h5>
                                    <hr>
										<div class="col-md-4 col-lg-4 col-xs-12">
					                        <div class="white-box text-center bg-info">
					                            <h1 class="text-white counter"><?php echo money_format('%#10n', $reg->votos); ?></h1>
					                            <p class="text-white">Votos</p>
					                        </div>
					                    </div>
					                    <div class="col-md-4 col-lg-4 col-xs-12">
					                        <div class="white-box text-center bg-success">
					                            <h1 class="text-white counter"><?php echo $reg->resultado; ?></h1>
					                            <p class="text-white">Resultado</p>
					                        </div>
					                    </div>
                                    </div>
                                </div>
                                <?php endforeach;?>
								<?php else:?>
								<p>No hay datos en la base de datos</p>
								<?php  endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>