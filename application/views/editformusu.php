<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <?php if($usuario): ?>
								  <?php $reg=$usuario->row() ?>
	                          	  <?php  echo form_open('/admin/editarUsuario', 'class="form-validate form-horizontal" id="formEditar"') ?>
	                                  <input type="hidden" name="user_id" class="form-control" value="<?php echo $reg->id; ?>">
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Nombre <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->nombre; ?>" name="name" minlength="5" type="text" required />
	                                      </div>
	                                  </div>       
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Usuario <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->username; ?>" name="username" minlength="6" type="text" required />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Contraseña </span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="password" name="password"/>
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Rol</label>
	                                      <div class="col-lg-10">
	                                          <select class="form-control" name="role" form="formEditar" required="required">
													<?php if ("Administrador"==$reg->rol){ ?>
														<option selected value="Administrador">Administrador</option>
														<option value="Funcionario">Funcionario</option>
														<option value="Usuario">Usuario</option>
													<?php } else if ("Usuario"==$reg->rol){ ?>
														<option value="Funcionario">Funcionario</option>
														<option value="Administrador">Administrador</option>
														<option selected value="Usuario">Usuario</option>
													<?php } else {?>
														<option value="Administrador">Administrador</option>
														<option selected value="Funcionario">Funcionario</option>
														<option value="Usuario">Usuario</option>
													<?php }?>
											  </select>
	                                      </div>
	                                  </div>                                                                    
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Correo <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="email" value="<?php echo $reg->correo; ?>" name="mail"/>
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Dirección <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="text" value="<?php echo $reg->direccion; ?>" name="direccion"/>
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Municipio</label>
	                                      <div class="col-lg-10">
	                                          <select class="form-control" name="municipio" form="formEditar" required="required">
													<option selected value="<?php echo $reg->municipio; ?>"><?php echo $reg->municipio; ?></option>
													<option value="Albán">Albán</option>
													<option value="Aldana">Aldana</option>
													<option value="Ancuya">Ancuya</option>
													<option value="Arboleda">Arboleda</option>
													<option value="Barbacoas">Barbacoas</option>
													<option value="Belén">Belén</option>
													<option value="Buesaco">Buesaco</option>
													<option value="Chachagüí">Chachagüí</option>
													<option value="Colón">Colón</option>
													<option value="Consacá">Consacá</option>
													<option value="Contadero">Contadero</option>
													<option value="Córdoba">Córdoba</option>
													<option value="Cuaspud">Cuaspud</option>
													<option value="Cumbal">Cumbal</option>
													<option value="Cumbitara">Cumbitara</option>
													<option value="El Charco">El Charco</option>
													<option value="El Peñol">El Peñol</option>
													<option value="El Rosario">El Rosario</option>
													<option value="El Tablón de Gómez">El Tablón de Gómez</option>
													<option value="El Tambo">El Tambo</option>
													<option value="Francisco Pizarro">Francisco Pizarro</option>
													<option value="Funes">Funes</option>
													<option value="Guachucal">Guachucal</option>
													<option value="Guaitarilla">Guaitarilla</option>
													<option value="Gualmatán">Gualmatán</option>
													<option value="Iles">Iles</option>
													<option value="Imués">Imués</option>
													<option value="Ipiales">Ipiales</option>
													<option value="La Cruz">La Cruz</option>
													<option value="La Florida">La Florida</option>
													<option value="La Llanada">La Llanada</option>
													<option value="La Tola">La Tola</option>
													<option value="La Unión">La Unión</option>
													<option value="Leiva">Leiva</option>
													<option value="Linares">Linares</option>
													<option value="Los Andes">Los Andes</option>
													<option value="Magüí Payán">Magüí Payán</option>
													<option value="Mallama">Mallama</option>
													<option value="Mosquera">Mosquera</option>
													<option value="Nariño">Nariño</option>
													<option value="Olaya Herrera">Olaya Herrera</option>
													<option value="Ospina">Ospina</option>
													<option value="Pasto">Pasto</option>
													<option value="Policarpa">Policarpa</option>
													<option value="Potosí">Potosí</option>
													<option value="Providencia">Providencia</option>
													<option value="Puerres">Puerres</option>
													<option value="Pupiales">Pupiales</option>
													<option value="Ricaurte">Ricaurte</option>
													<option value="Roberto Payán">Roberto Payán</option>
													<option value="Samaniego">Samaniego</option>
													<option value="San Bernardo">San Bernardo</option>
													<option value="San Lorenzo">San Lorenzo</option>
													<option value="San Pablo">San Pablo</option>
													<option value="San Pedro de Cartago">San Pedro de Cartago</option>
													<option value="Sandoná">Sandoná</option>
													<option value="Santa Bárbara">Santa Bárbara</option>
													<option value="Santacruz">Santacruz</option>
													<option value="Sapuyes">Sapuyes</option>
													<option value="Taminango">Taminango</option>
													<option value="Tangua">Tangua</option>
													<option value="Tumaco">Tumaco</option>
													<option value="Túquerres">Túquerres</option>
													<option value="Yacuanquer">Yacuanquer</option>
											  </select>
	                                      </div>
	                                  </div>      
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Celular <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="number" value="<?php echo $reg->celular; ?>" name="cellphone"/>
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Estado</label>
	                                      <div class="col-lg-10">
	                                          <select class="form-control" name="estado" form="formEditar" required="required">
													<?php if ("Activo"==$reg->estado){ ?>
														<option selected value="Activo">Activo</option>
														<option value="Inactivo">Inactivo</option>
													<?php } else {?>
														<option value="Activo">Activo</option>
														<option selected value="Inactivo">Inactivo</option>
													<?php }?>
											  </select>
	                                      </div>
	                                  </div> 
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Saldo <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="number" value="<?php echo $reg->saldo; ?>" name="saldo" required />
	                                      </div>
	                                  </div>
	                                  <div class="form-group">
	                                      <div class="col-lg-offset-2 col-lg-10">
	                                          <button class="btn btn-primary" type="submit">Editar</button>
	                                      </div>
	                                  </div>
	                              <?php echo form_close() ?>
	                              <?php else:?>
								  <p>No hay datos en la base de datos</p>
								  <?php  endif; ?>
                        </div>
                    </div>