<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <?php if($registros): ?>
								  <?php $reg=$registros->row() ?>
	                          	  <?php  echo form_open('/admin/editarMunicipio', 'class="form-validate form-horizontal" id="formEditar"') ?>
	                                  <input type="hidden" name="muni_id" class="form-control" value="<?php echo $reg->id; ?>">
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Municipio <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input disabled class="form-control" value="<?php echo $reg->municipio; ?>" name="municipio" type="text"/>
	                                      </div>
	                                  </div>     
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Año <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input disabled class="form-control" value="<?php echo $reg->anio; ?>" name="anio" type="number" />
	                                      </div>
	                                  </div>     
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Eficacia <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->eficacia; ?>" name="eficacia" step="any" type="number" />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Eficiencia </span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->eficiencia; ?>" name="eficiencia" step="any" type="number" />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Requisitos Legales </span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->requisitos; ?>" name="requisitos" step="any" type="number" />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Gestión Administrativa </span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->gestion; ?>" name="gestion" step="any" type="number" />
	                                      </div>
	                                  </div>
	                                  <div class="form-group">
	                                      <div class="col-lg-offset-2 col-lg-10">
	                                          <button class="btn btn-primary" type="submit">Editar</button>
	                                      </div>
	                                  </div>
	                              <?php echo form_close() ?>
	                              <?php else:?>
								  <p>No hay datos en la base de datos</p>
								  <?php  endif; ?>
                        </div>
                    </div>