<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <?php if($registros): ?>
								  <?php $reg=$registros->row() ?>
	                          	  <?php  echo form_open('/usu/editarUsuario', 'class="form-validate form-horizontal" id="formEditar"') ?>
	                                  <input type="hidden" name="user_id" class="form-control" value="<?php echo $reg->id; ?>">
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Nombre <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" value="<?php echo $reg->nombre; ?>" name="name" minlength="5" type="text" required />
	                                      </div>
	                                  </div>  
	                                  <input type="hidden" name="username" class="form-control" value="<?php echo $reg->username; ?>">
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Usuario <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input disabled class="form-control" value="<?php echo $reg->username; ?>" name="usernametemp" minlength="6" type="text" required />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Contraseña </span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="password" name="password"/>
	                                      </div>
	                                  </div>
	                                  <input type="hidden" name="role" class="form-control" value="<?php echo $reg->rol; ?>">                                                                 
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Correo <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="email" value="<?php echo $reg->correo; ?>" name="mail"/>
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Dirección <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="text" value="<?php echo $reg->direccion; ?>" name="direccion"/>
	                                      </div>
	                                  </div>
	                                  <input type="hidden" name="municipio" class="form-control" value="<?php echo $reg->municipio; ?>">
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Municipio <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input disabled class="form-control" type="text" value="<?php echo $reg->municipio; ?>" name="municipiotemp"/>
	                                      </div>
	                                  </div>     
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Celular <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="number" value="<?php echo $reg->celular; ?>" name="cellphone"/>
	                                      </div>
	                                  </div>
	                                  <input type="hidden" name="estado" class="form-control" value="<?php echo $reg->estado; ?>">	                                  
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Saldo <span class="required"></span></label>
	                                      <div class="col-lg-10">
	                                          <input disabled class="form-control" type="number" value="<?php echo $reg->saldo; ?>" name="saldotemp" />
	                                      </div>
	                                  </div>
	                                  <input type="hidden" name="saldo" class="form-control" value="<?php echo $reg->saldo; ?>">
	                                  <div class="form-group">
	                                      <div class="col-lg-offset-2 col-lg-10">
	                                          <button class="btn btn-primary" type="submit">Editar</button>
	                                      </div>
	                                  </div>
	                                  <!--<h3>Para que los cambios se puedan efectuar debes volver a iniciar sesión</h3>-->
	                              <?php echo form_close() ?>
	                              <?php else:?>
								  <p>No hay datos en la base de datos</p>
								  <?php  endif; ?>
                        </div>
                    </div>