<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <!-- a class="logo" href="<?php echo site_url('dashboard') ?>"><b><img src="<?php echo base_url()."assets/images/maple-icon.png"?>" alt="Home"></b><span class="hidden-xs"><img src="<?php echo base_url()."assets/images/maple-admin.png"?>" alt="Home"></span></a -->
                    <a class="logo hidden-xs" href="<?php echo site_url('dashboard') ?>"><b></b><span class="hidden-xs"><img src="<?php echo base_url()."assets/images/maple-admin.png"?>" alt="Home"></span></a>
                </div>
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <div class="navbar-default sidebar nicescroll" role="navigation">
            <div class="sidebar-nav navbar-collapse ">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="nav-small-cap">Menú Principal</li>
                    <li> <a href="<?php echo site_url('Gral/verZonas') ?>" class="waves-effect"><i class="icon-speedometer fa-fw"></i> Mapa</a>
                    
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        