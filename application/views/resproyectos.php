<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Resultado de Proyectos</h3>
                            <?php if($registros): ?>          
			                <table class="table table-striped table-advance table-hover" id="example">
			                	  <thead>
			                      <tr>
			                         <th><i class="icon_pencil"></i> Nombre</th>
			                         <th><i class="icon_profile"></i> Valor</th>
			                         <th><i class="icon_tag"></i> Municipio</th>  
			                         <th><i class="icon_mobile"></i> Votos</th>
			                         <th><i class="icon_mobile"></i> Resultado</th>
			                      </tr>
			                      </thead>
			                      <tbody>
			                      <?php foreach($registros->result() as $reg): ?>
									<tr>
										<td><p align=left><?php echo $reg->nombre; ?></p></td>
										<td><p align=right><?php echo "$ ".number_format($reg->valor, 0, '', '.') ?></p></td>
										<td><p align=left><?php echo $reg->municipio; ?></p></td>
										<td><p align=left><?php echo $reg->votos; ?></p></td>
										<td>
											<p align=left>
												<?php 
												if ($reg->resultado=="SI") {
													?><button type="submit" disabled class="btn btn-success btn-sm">SI</button>
													<?php 
												}
												elseif($reg->resultado=="NO"){
													?><button type="submit" disabled class="btn btn-danger btn-sm">NO</button>
													<?php 
												}
												else{
													?>-
													<?php 
												}
												?>
											</p>
										</td>
									</tr>
								  <?php endforeach;?>
								  </tbody>
			                </table>
							<?php else:?>
							<p>No hay datos en la base de datos</p>
							<?php  endif; ?>
                        </div>
                    </div>
