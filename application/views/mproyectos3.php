<script> 
	function sumar(num) {

	  var total = 0;
	  $(".monto").each(function() {
	    if (isNaN(parseFloat($(this).val()))) {
	      total += 0;
	    } else {
	      total += parseFloat($(this).val());
	    }
	  });
	  //alert(total);
	  document.getElementById('spTotal').innerHTML = total;
	  if (document.getElementById('totalf').value<total) {
	  	document.getElementById("btn_votar").disabled = true;	
	  	swal("Saldo Insuficiente", "¡El valor de los proyectos debe ser menor!", "warning");
	  }
	  else{
	  	document.getElementById("btn_votar").disabled = false;		
	  };
	  
	}
	function poners(valor, num) {
	  document.getElementById('i'+num).value = valor;
	  document.getElementById("ba"+num).disabled = true;
	  sumar();
	}
	function ponerq(valor, num) {
	  document.getElementById('i'+num).value = valor;
	  document.getElementById("ba"+num).disabled = false;
	  sumar();
	}
</script>
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
                        	<?php $saldo=$saldousuario->row() ?>
                            <h3>Saldo Actual: $<strong><?php echo money_format('%#10n', $saldo->saldo); ?></strong><br>
                            <input hidden id="totalf" value="<?php echo $saldo->saldo; ?>" name="saldo"><br>
                             Gastado: $<strong><span id="spTotal"></span></strong><br>
                            <hr>
                            <div class="row">
                                <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 mail_listing">                                    
                                    <div class="message-center">
                                    	
                                    	<?php if($registros): ?> 
                                    		<div class="row">
                                    		<?php $sum = 1; ?>
                                    		<?php  echo form_open('/usu/agregarVoto', 'class="form-validate form-horizontal" name="formRegistrar" id="formRegistrar"') ?>
						                    <?php foreach($registros->result() as $reg): ?>
						                    	<?php if ($saldo->saldo>=$reg->valor): ?>						                    		
									                    <div class="col-md-3 col-xs-12 col-sm-6"> <img class="img-responsive" alt="user" src="<?php echo $reg->adjunto; ?>">
									                        <div class="white-box">
									                            <!--<div class="text-muted"><span class="m-r-10"><?php echo $reg->estado; ?></span></div>-->
									                            <h3 class="m-t-20 m-b-20"><?php echo $reg->nombre; ?></h3>
									                            <p><?php echo money_format('%#10n', $reg->valor) ?></p>									                            
											                    	<input type="hidden" name="<?php echo "proy_id".$sum; ?>" class="form-control" value="<?php echo $reg->id; ?>">	
											                    	<input type="hidden" name="<?php echo "user_id".$sum; ?>" class="form-control" value="<?php echo $saldo->id; ?>">												                    	
											                        <input onchange="sumar();"  id="<?php echo "i".$sum; ?>" class="monto" value="0" type="hidden" min="0" required="" placeholder="Ingrese el monto" name="<?php echo "monto".$sum; ?>"><br>
											                        <input type="button" id="<?php echo "ba".$sum; ?>" class="btn btn-info" value="Agregar" onclick="poners(<?php echo $reg->valor; ?>, <?php echo $sum; ?>)">
											                    	<input type="button" id="<?php echo "bq".$sum; ?>" class="btn btn-danger" value="Quitar" onclick="ponerq(<?php echo 0; ?>, <?php echo $sum; ?>)">
											                </div>
									                    </div>
						                    	<?php endif ?>	
						                    	<?php $sum=$sum+1; ?>					                    													
											<?php endforeach;?>											
											</div>
											<button class="btn btn-success waves-effect waves-light" id="btn_votar" type="submit"><span class="btn-label"><i class="fa fa-check"></i></span>Votar</button>
											</form>
										<?php else:?>
										<p>No hay datos en la base de datos</p>
										<?php  endif; ?>
										
                                        <!--
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status online pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Pavan kumar</h5>
                                                <span class="mail-desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span> <span class="time">9:30 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        <a href="inbox-detail.html" class="unread">
                                            <div class="user-img"><span class="profile-status busy pull-right"></span> </div>
                                            <div class="mail-contnet">
                                                <h5>Sonu Nigam</h5>
                                                <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM <i class="icon-paper-clip m-l-5"></i></span> </div>
                                        </a>
                                        -->
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        
