<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Gestión de Proyectos</h3>
                            <a class="btn btn-primary" href="<?php echo site_url('func/formularioRegistrarProyecto') ?>" title="Bootstrap 3 themes generator">
          					<span class="icon_plus_alt2"></span> Registrar Proyecto</a><br><br>
                            <?php if($registros): ?>          
			                <table class="table table-striped table-advance table-hover" id="example">
			                	  <thead>
			                      <tr>
			                         <th><i class="icon_pencil"></i> Nombre</th>
			                         <th><i class="icon_profile"></i> Valor</th>
			                         <th><i class="icon_tag"></i> Municipio</th>  
			                         <th><i class="icon_mail_alt"></i> Tiempo</th>
			                         <th><i class="icon_mobile"></i> Votos</th>
			                         <th><i class="icon_cursor_alt"></i> Estado</th>
			                         <th><i class="icon_cogs"></i> Acción</th>
			                      </tr>
			                      </thead>
			                      <tbody>
			                      <?php foreach($registros->result() as $reg): ?>
									<tr>
										<td><p align=left><?php echo $reg->nombre; ?></p></td>
										<td><p align=left><?php echo $reg->valor ?></p></td>
										<td><p align=left><?php echo $reg->municipio; ?></p></td>	
										<td><p align=left><?php echo $reg->tiempo; ?></p></td>
										<td><p align=left><?php echo $reg->votos; ?></p></td>
										<td>
											<p align=left>
												<?php 
												if ($reg->estado=="Abierto") {
													?><button type="submit" disabled class="btn btn-success btn-sm">Abierto</button>
													<?php 
												}
												else{
													?><button type="submit" disabled class="btn btn-danger btn-sm">Cerrado</button>
													<?php 
												}
												?>
											</p>
										</td>
										<td>
				                        <div class="btn-group">
					                        <?php  echo form_open('/func/formularioEditarProyecto', 'class="form-horizontal" id="formEditar"') ?>
												<input type="hidden" name="id_proy" value="<?php echo $reg->id; ?>">
												<button type="submit" class="btn btn-info btn-sm">Editar</button>
											<?php echo form_close() ?>
										</div>
			                         </td>
									</tr>
								  <?php endforeach;?>
								  </tbody>
			                </table>
							<?php else:?>
							<p>No hay datos en la base de datos</p>
							<?php  endif; ?>
                        </div>
                    </div>