<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
	                      	  <?php  echo form_open('/admin/verDatosAnio', 'class="form-validate form-horizontal" id="formEditar"') ?>
	                              <div class="form-group ">
	                                  <label class="control-label col-lg-2">Año</label>
	                                  <div class="col-lg-10">
	                                      <select class="form-control" name="anio" form="formEditar" required="required">
												<option value="2015">2015</option>
												<option value="2016">2016</option>
												<option selected value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
										  </select>
	                                  </div>
	                              </div>  
	                              <div class="form-group">
	                                  <div class="col-lg-offset-2 col-lg-10">
	                                      <button class="btn btn-primary" type="submit">Ver</button>
	                                  </div>
	                              </div>
	                          <?php echo form_close() ?>
                        </div>
                    </div>