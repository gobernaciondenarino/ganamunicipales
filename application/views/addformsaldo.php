		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3>Asignación de Montos</h3>
                            <?php  echo form_open('/func/asignarSaldo', 'class="form-validate form-horizontal" id="formRegistrar"') ?>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Valor <span class="required">*</span></label>
	                                      <div class="col-lg-10">
	                                          <input class="form-control" type="money" name="valor" required />
	                                      </div>
	                                  </div>
	                                  <div class="form-group ">
	                                      <label class="control-label col-lg-2">Municipio</label>
	                                      <div class="col-lg-10">
	                                          <select class="form-control" name="municipio" form="formRegistrar" required="required">
													<option value="Albán">Albán</option>
													<option value="Aldana">Aldana</option>
													<option value="Ancuya">Ancuya</option>
													<option value="Arboleda">Arboleda</option>
													<option value="Barbacoas">Barbacoas</option>
													<option value="Belén">Belén</option>
													<option value="Buesaco">Buesaco</option>
													<option value="Chachagüí">Chachagüí</option>
													<option value="Colón">Colón</option>
													<option value="Consacá">Consacá</option>
													<option value="Contadero">Contadero</option>
													<option value="Córdoba">Córdoba</option>
													<option value="Cuaspud">Cuaspud</option>
													<option value="Cumbal">Cumbal</option>
													<option value="Cumbitara">Cumbitara</option>
													<option value="El Charco">El Charco</option>
													<option value="El Peñol">El Peñol</option>
													<option value="El Rosario">El Rosario</option>
													<option value="El Tablón de Gómez">El Tablón de Gómez</option>
													<option value="El Tambo">El Tambo</option>
													<option value="Francisco Pizarro">Francisco Pizarro</option>
													<option value="Funes">Funes</option>
													<option value="Guachucal">Guachucal</option>
													<option value="Guaitarilla">Guaitarilla</option>
													<option value="Gualmatán">Gualmatán</option>
													<option value="Iles">Iles</option>
													<option value="Imués">Imués</option>
													<option value="Ipiales">Ipiales</option>
													<option value="La Cruz">La Cruz</option>
													<option value="La Florida">La Florida</option>
													<option value="La Llanada">La Llanada</option>
													<option value="La Tola">La Tola</option>
													<option value="La Unión">La Unión</option>
													<option value="Leiva">Leiva</option>
													<option value="Linares">Linares</option>
													<option value="Los Andes">Los Andes</option>
													<option value="Magüí Payán">Magüí Payán</option>
													<option value="Mallama">Mallama</option>
													<option value="Mosquera">Mosquera</option>
													<option value="Nariño">Nariño</option>
													<option value="Olaya Herrera">Olaya Herrera</option>
													<option value="Ospina">Ospina</option>
													<option value="Pasto">Pasto</option>
													<option value="Policarpa">Policarpa</option>
													<option value="Potosí">Potosí</option>
													<option value="Providencia">Providencia</option>
													<option value="Puerres">Puerres</option>
													<option value="Pupiales">Pupiales</option>
													<option value="Ricaurte">Ricaurte</option>
													<option value="Roberto Payán">Roberto Payán</option>
													<option value="Samaniego">Samaniego</option>
													<option value="San Bernardo">San Bernardo</option>
													<option value="San Lorenzo">San Lorenzo</option>
													<option value="San Pablo">San Pablo</option>
													<option value="San Pedro de Cartago">San Pedro de Cartago</option>
													<option value="Sandoná">Sandoná</option>
													<option value="Santa Bárbara">Santa Bárbara</option>
													<option value="Santacruz">Santacruz</option>
													<option value="Sapuyes">Sapuyes</option>
													<option value="Taminango">Taminango</option>
													<option value="Tangua">Tangua</option>
													<option value="Tumaco">Tumaco</option>
													<option value="Túquerres">Túquerres</option>
													<option value="Yacuanquer">Yacuanquer</option>
											  </select>
	                                      </div>
	                                  </div> 
	                                  <div class="form-group">
	                                      <div class="col-lg-offset-2 col-lg-10">
	                                          <button class="btn btn-primary" type="submit">Asignar</button>
	                                      </div>
	                                  </div>
	                              <?php echo form_close() ?>
                        </div>
                    </div>