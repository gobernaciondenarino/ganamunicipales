<body>
    <section id="wrapper" class="login-register">
        <div class="container text-center">
            <div class="login-box">
                <div class="white-box">                    
                <?php $u=$usuario; ?>
                <?php $p=$clave; ?>
                <?php $error = $this->session->flashdata("error"); ?>
                        <div class="alert alert-<?php echo $error ? 'warning' : 'info' ?> alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <?php echo $error ? $error : 'Ingresa tus datos' ?>
                        </div>
                        <div class="form-group ">
                        <div class="col-xs-12">
                            <img src="http://192.168.88.235/municipal/imgproy/logo-4.png" alt="GANA MUNICIPAL" class="normal_logo">
                            <!-- h3>Gana Municipal</h3 -->
                            <p class="text-muted">Ingresa aquí para votar!</p>
                        </div>
                        </div>
                        <?php  echo form_open('/auth') ?>
                        <?php $error = form_error("username", "<p class='text-danger'>", '</p>'); ?>              
                        <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
                        <div class="input-group col-xs-12">
                            <input type="hidden" name="username" value="<?php echo $u; ?>" id="username" class="form-control" placeholder="Cédula">
                        </div>  
                        <?php echo $error; ?>
                        </div>
                        <?php $error = form_error("password", "<p class='text-danger'>", '</p>'); ?>
                        <div class="form-group <?php echo $error ? 'has-error' : '' ?>">
                        <div class="input-group col-xs-12">
                            <input type="hidden" name="password" value="<?php echo $p; ?>" id="password" class="form-control" placeholder="Contraseña">
                        </div> 
                        <?php echo $error; ?>
                        </div>
                        <input type="submit" value="Empezar" class="btn btn-success btn-lg btn-block  waves-light">
                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12 text-center">
                                <p>¿Aún no quieres ingresar? <a href="<?php echo site_url('auth') ?>" class="text-primary m-l-5"><b>Salir</b></a></p>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                </div>
                <!-- footer class="footer text-center">
                </footer -->
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="<?php echo base_url()."assets/bower_components/jquery/dist/jquery.min.js"?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()."assets/bower_components/bootstrap/dist/js/bootstrap.min.js"?>"></script>
