<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
	                      	  <?php  echo form_open('/gral/verMapaSubregion', 'class="form-validate form-horizontal" id="formEditar"') ?>
	                              <div class="form-group ">
	                                  <label class="control-label col-lg-2">Año</label>
	                                  <div class="col-lg-10">
	                                      <select class="form-control" name="anio" form="formEditar" required="required">
												<option value="2015">2015</option>
												<option value="2016">2016</option>
												<option selected value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
										  </select>
	                                  </div>
	                              </div>  
	                              <div class="form-group ">
	                                  <label class="control-label col-lg-2">Subregión</label>
	                                  <div class="col-lg-10">
	                                      <select class="form-control" name="subregion" form="formEditar" required="required">
												<option value="Sanquianga">1 - Sanquianga</option>
												<option value="Pacifico Sur">2 -Pacifico Sur</option>
												<option value="Telembí">3 -Telembí</option>
												<option value="Pie de Monte Costero">4 - Pie de Monte Costero</option>
												<option value="Exprovincia de Obando">5 - Exprovincia de Obando</option>
												<option value="Sabana">6 - Sabana</option>
												<option value="Abades">7 - Abades</option>
												<option value="Occidente">8 - Occidente</option>
												<option value="Cordillera">9 - Cordillera</option>
												<option value="Centro">10 - Centro</option>
												<option value="Juanambú">11 - Juanambú</option>
												<option value="Rio Mayo">12 - Rio Mayo</option>
												<option value="Guambuyaco">13 - Guambuyaco</option>
										  </select>
	                                  </div>
	                              </div>  
	                              <div class="form-group">
	                                  <div class="col-lg-offset-2 col-lg-10">
	                                      <button class="btn btn-primary" type="submit">Ver</button>
	                                  </div>
	                                  <div class="col-lg-offset-2 col-lg-10">
	                                  	<img id="subregion_img" src="<?php echo base_url()."assets/images/subregiones/Mapanar.png"?>"/>
	                                  </div>
	                              </div>
	                          <?php echo form_close() ?>
                        </div>
                        <li class="success">                        
