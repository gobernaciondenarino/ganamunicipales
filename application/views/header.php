<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()."assets/images/favicon.png"?>">
    <title>Gana Municipal - Gobernación de Nariño</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()."assets/bower_components/bootstrap/dist/css/bootstrap.min.css"?>" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url()."assets/bower_components/metisMenu/dist/metisMenu.min.css"?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?php echo base_url()."assets/bower_components/chartist-js/dist/chartist.min.css"?>" rel="stylesheet">
    <link href="<?php echo base_url()."assets/bower_components/chartist-js/dist/chartist-init.css"?>" rel="stylesheet">
    <link href="<?php echo base_url()."assets/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css"?>" rel="stylesheet">
    <link href="<?php echo base_url()."assets/bower_components/vectormap/jquery-jvectormap-2.0.2.css"?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()."assets/bower_components/metisMenu/dist/metisMenu.min.css"?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()."assets/bower_components/dropify/dist/css/dropify.min.css"?>">
    <link href="<?php echo base_url()."assets/css/style.css"?>" rel="stylesheet">
    
    <!--
    Sweet
    -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
    </head>
