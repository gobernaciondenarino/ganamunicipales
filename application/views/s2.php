<?php if($registros): ?>  
	<?php foreach($registros->result() as $reg): ?>
		<?php
		if ($reg->municipio=="Francisco Pizarro") {
			$francisco=$reg->etiqueta;
		}
		elseif ($reg->municipio=="Tumaco") {
			$tumaco=$reg->etiqueta;
		}
		?>
	<?php endforeach;?>
<?php else:?>
<p>No hay datos en la base de datos</p>
<?php  endif; ?>
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <h4 class="page-title">Gobernación de Nariño</h4>
                        <ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
							<script type="text/javascript">
						        //---------------------------------------------------------------------------------
						        function enter(area) {
						            switch (area) {
						                case 1: swal("<?php echo "Calificación: ".$francisco; ?>", "Indicador de Desempeño Integral", "warning"); break;
						                case 2: swal("<?php echo "Calificación: ".$tumaco; ?>", "Indicador de Desempeño Integral", "warning"); break;
						            }
						        }
						        //---------------------------------------------------------------------------------
						    </script>
						    <style>
						        .TextStyle {
						            font-family: Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;
						            font-size: 1.1em;
						        }
						    </style>
						    <table border="0" width="750">
						        <tr>
						            <td align="left">
						                <b>Pacifico Sur</b>
						            </td>
						        </tr>
						    </table>
						    <hr />
						    <table border="0" width="750">
						        <tr>
						            <td align="center">
						                <img id="subregion_img" src="<?php echo base_url()."assets/images/subregiones/subregion2.png"?>" usemap="#test" />
						                <map id="test" name="test">
						                    <area id="sh1" shape="poly" alt="Francisco Pizarro" title="Francisco Pizarro" coords="156,2,163,10,163,22,159,27,164,27,164,37,161,42,169,48,173,46,190,58,199,69,208,69,202,77,194,116,186,133,175,133,175,155,180,157,172,168,175,179,172,195,157,200,151,196,148,176,155,167,141,158,134,165,123,167,125,153,116,118,126,118,127,111,114,116,109,98,114,83,122,64,124,70,131,65,125,58,138,43,146,29,140,20" href="#" onclick="enter(1)" />
						                    <area id="sh2" shape="poly" alt="Tumaco" title="Tumaco" coords="186,128,182,158,190,172,188,182,196,192,200,210,208,222,198,230,198,240,220,250,228,270,224,292,234,302,226,316,244,318,250,306,260,306,260,302,264,384,252,376,250,368,196,378,192,388,194,398,186,438,160,436,134,418,130,398,86,376,72,356,78,342,56,342,50,328,64,318,54,312,42,324,12,300,12,274,56,226,104,222,104,240,146,230,154,240,164,240,172,196,166,194,174,182,170,164" href="#" onclick="enter(2)" />
						                </map>
						                <script>
						                    (function ($) {
						                        jQuery("#sh1").mouseenter(function () {
						                            jQuery("#municipality_title").text("Municipio: Francisco Pizarro");
						                        });
						                        jQuery("#sh2").mouseenter(function () {
						                            jQuery("#municipality_title").text("Municipio: Tumaco");
						                        });
						                    })(jQuery);
						                 </script>
						            </td>
						        </tr>
						    </table>
                        </div>
                    </div>