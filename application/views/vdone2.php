<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-12">
                        <!-- h4 class="page-title">Gobernación de Nariño</h4 -->
                        <!-- ol class="breadcrumb">
                            <li><a href="#">Gana Municipal</a></li>
                        </ol -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <!-- Left sidebar -->
                    <div class="col-md-12">
                        <div class="white-box">
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
                            <?php $saldo=$saldousuario->row() ?>
                            
                            <?php  echo form_open('/usu/encuesta', 'class="form-validate form-horizontal" id="formRegistrar"') ?>
                            <input  type="hidden" name="municipio" class="form-control" value="<?php echo $saldo->municipio; ?>">
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <h3><strong>Encuesta</strong></h3><br>
                                        <h3>¿Cual cree usted que es el principal desafío que se debe resolver en su municipio?</h3>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                        <input required class="form-control"  name="respuesta">
                                    </div>
                                </div>
                                <div class="form-group text-center m-t-10">
                                    <div class="col-xs-12">
                                        <button class="btn btn-info btn-lg btn-block waves-light" type="submit">Finalizar Votación</button>
                                    </div>
                                </div>
                            </form>
                             </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
