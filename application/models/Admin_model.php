﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	function __construct (){
		parent::__construct();
		$this->load->database();
	}
	public function obtenerUsuarios(){
		$this->db->select('*');
		$this->db->from('user');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}	
	public function obtenerProyectos(){
		$this->db->select('*');
		$this->db->from('proyectos');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}	
	public function obtenerUsuariosActivos(){
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where('estado', 'Activo');
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerMunicipios($anio){
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->where('municipios.anio', $anio);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function tarjetonUsuario($user, $municipio){
		$this->db->query("insert ignore into relpu(proyecto_id, user_id) select proyectos.id, ".$user." from proyectos where proyectos.estado="."'Abierto'"." and proyectos.municipio='".$municipio."'");
	}
	public function obtenerTarjeton($user){
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->join('relpu', 'relpu.proyecto_id = proyectos.id');
		$this->db->where('relpu.user_id', $user);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerNumProyectos($user){
		$this->db->select_sum('relpu.agregado');
		$this->db->from('proyectos');
		$this->db->join('relpu', 'relpu.proyecto_id = proyectos.id');
		$this->db->where('relpu.user_id', $user);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerTotProyectos($user){
		$this->db->select('count(relpu.agregado) as tot');
		$this->db->from('proyectos');
		$this->db->join('relpu', 'relpu.proyecto_id = proyectos.id');
		$this->db->where('relpu.user_id', $user);
		$this->db->where('relpu.agregado', 0);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerMunicipio($muni, $anio){
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->where('municipios.anio', $anio);
		$this->db->where('municipios.id', $muni);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerUsuario($proy){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.id', $proy);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerProyecto($user){
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->where('proyectos.id', $user);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerProyectosM($municipio){
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->where('proyectos.municipio', $municipio);
		$this->db->where('proyectos.estado', "Abierto");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function obtenerProyectosC($municipio){
		$this->db->select('*');
		$this->db->from('proyectos');
		$this->db->where('proyectos.municipio', $municipio);
		$this->db->order_by("proyectos.votos", "desc");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query;
		}
		else{
			return false;
		}
	}
	public function buscarUsu($usu){
	    $this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.username', $usu);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return 1;
		}
		else{
			return 0;
		}
	}
	public function encuestar($res, $muni){
		$this->db->insert('encuestas', array('municipio' => $muni, 'respuesta' => $res));
		$lastId=$this->db->insert_id();		
	}
	public function nuevoUsuario($data){
		$this->db->insert('user', array('saldo' => '800000000', 'username' => $data['username'], 'password' => sha1($data['password']), 'rol' => $data['rol'], 'nombre' => $data['nombre'], 'apellido' => $data['apellido'], 'correo' => $data['correo'], 'estado' => $data['estado'], 'celular' => $data['celular'], 'municipio' => $data['municipio']));
		$lastId=$this->db->insert_id();		
	}
	public function nuevoProyecto($data){
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' => $data['municipio'], 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$lastId=$this->db->insert_id();		
	}public function nuevoProyectoT($data){
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Albán", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Aldana", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Ancuya", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Arboleda", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Barbacoas", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Belén", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Buesaco", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Chachagüí", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Colón", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Consacá", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Contadero", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Córdoba", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Cuaspud", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Cumbal", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Cumbitara", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"El Charco", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"El Peñol", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"El Rosario", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"El Tablón de Gómez", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"El Tambo", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Francisco Pizarro", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Funes", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Guachucal", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Guaitarilla", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Gualmatán", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Iles", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Imués", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Ipiales", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"La Cruz", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"La Florida", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"La Llanada", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"La Tola", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"La Unión", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Leiva", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Linares", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Los Andes", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Magüí Payán", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Mallama", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Mosquera", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Nariño", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Olaya Herrera", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Ospina", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Pasto", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Policarpa", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Potosí", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Providencia", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Puerres", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Pupiales", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Ricaurte", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Roberto Payán", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Samaniego", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"San Bernardo", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"San Lorenzo", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"San Pablo", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"San Pedro de Cartago", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Sandoná", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Santa Bárbara", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Santacruz", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Sapuyes", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Taminango", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Tangua", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Tumaco", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Túquerres", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$this->db->insert('proyectos', array('nombre' => $data['nombre'], 'descripcion' => $data['descripcion'], 'valor' => $data['valor'], 'municipio' =>"Yacuanquer", 'autor' => $data['autor'], 'tiempo' => $data['tiempo'], 'fechaini' => $data['fechaini'], 'fechafin' => $data['fechafin'], 'votos' => $data['votos'], 'resultado' => $data['resultado'], 'estado' => $data['estado'], 'adjunto' => $data['adjunto']));
		$lastId=$this->db->insert_id();		
	}
	public function editarUsu($id, $data){
		if ($data['password']=="") {
			$ue = array(
			'nombre' => $data['name'],
			'apellido' => $data['lastname'],
			'username' => $data['username'],
			'rol' => $data['role'],
			'correo' => $data['mail'],
			'direccion' => $data['direccion'],
			'celular' => $data['cellphone'],
			'municipio' => $data['municipio'],
			'estado' => $data['estado'],
			'saldo' => $data['saldo']
			);
		}
		else{
			$ue = array(
			'nombre' => $data['name'],
			'apellido' => $data['lastname'],
			'username' => $data['username'],
			'password' => sha1($data['password']),
			'rol' => $data['role'],
			'correo' => $data['mail'],
			'direccion' => $data['direccion'],
			'celular' => $data['cellphone'],
			'municipio' => $data['municipio'],
			'estado' => $data['estado'],
			'saldo' => $data['saldo']
			);
		}
		$this->db->where('id', $id);
		$this->db->update('user', $ue);
	}	
	public function editarUsuDone($id){
		$ue = array(
			'rol' => 'Done'
		);
		$this->db->where('id', $id);
		$this->db->update('user', $ue);
	}	
	public function editarProy($id, $data){
		$ue = array(
			'nombre' => $data['nombre'],
			'descripcion' => $data['descripcion'],
			'valor' => $data['valor'],
			'municipio' => $data['municipio'],
			'autor' => $data['autor'],
			'tiempo' => $data['tiempo'],
			'fechaini' => $data['fechaini'],
			'fechafin' => $data['fechafin'],
			'votos' => $data['votos'],
			'resultado' => $data['resultado'],
			'estado' => $data['estado'],
			'adjunto' => $data['adjunto']
			);
		$this->db->where('id', $id);
		$this->db->update('proyectos', $ue);
	}	
	public function editarMuni($id, $data){
		$ue = array(
			'eficacia' => $data['eficacia'],
			'eficiencia' => $data['eficiencia'],
			'requisitos' => $data['requisitos'],
			'gestion' => $data['gestion'],
			'integral' => $data['integral'],
			'etiqueta' => $data['etiqueta']
			);
		$this->db->where('id', $id);
		$this->db->update('municipios', $ue);
	}	
	public function realizarVoto($proy, $user){
		echo $proy;
		echo "---------- ".$user;
		$this->db->query("update proyectos set votos=votos+1 where id=".$proy);
		$this->db->query("update relpu set agregado='1' where relpu.proyecto_id=".$proy." and relpu.user_id=".$user);
	}
	public function eliminarVoto($proy, $user){
		echo $proy;
		echo "---------- ".$user;
		$this->db->query("update proyectos set votos=votos-1 where id=".$proy);
		$this->db->query("update relpu set agregado='0' where relpu.proyecto_id=".$proy." and relpu.user_id=".$user);
	}
	public function restarSaldo($id, $monto){
		$this->db->query("update user set saldo=saldo-".$monto." where id=".$id);
	}
	public function sumarSaldo($id, $monto){
		$this->db->query("update user set saldo=saldo+".$monto." where id=".$id);
	}
	public function asignarSaldoUsuarios($monto, $municipio){
		$this->db->query("update user set saldo=".$monto." where municipio='".$municipio."'");
	}
}
