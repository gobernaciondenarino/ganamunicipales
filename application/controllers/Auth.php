<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for (all) non logged in users
 */
class Auth extends MY_Controller {	

	public function logged_in_check()
	{
		if ($this->session->userdata("logged_in")) {
			redirect("dashboard");
		}
	}

	public function index()
	{	
		$this->logged_in_check();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules("username", "Username", "trim|required");
		$this->form_validation->set_rules("password", "Password", "trim|required");
		if ($this->form_validation->run() == true) 
		{
			$this->load->model('auth_model', 'auth');	
			// check the username & password of user
			$status = $this->auth->validate();
			if ($status == ERR_INVALID_USERNAME) {
				$this->session->set_flashdata("error", "El usuario está incorrecto");
			}
			elseif ($status == ERR_INVALID_PASSWORD) {
				$this->session->set_flashdata("error", "La contraseña está incorrecta");
			}
			else
			{
				// success
				// store the user data to session
				$this->session->set_userdata($this->auth->get_data());
				$this->session->set_userdata("logged_in", true);
				// redirect to dashboard
				redirect("dashboard");
			}
		}

		$this->load->view("header");		
		$this->load->view("auth");
		$this->load->view("footer");
	}

	public function logout()
	{
		$this->session->unset_userdata("logged_in");
		$this->session->sess_destroy();
		redirect("auth");
	}
	public function formularioRegistrarUsuario()
	{
		$this->load->view("header");		
		$this->load->view("addformuser");
		$this->load->view("footer");
	}
	public function agregarUsuario(){
		$this->load->model('Admin_model');
		if ($this->input->post('username')==null or $this->input->post('password')==null or $this->input->post('nombre')==null or $this->input->post('municipio')==null or strlen($this->input->post('username'))<=4)
		{
		    redirect("auth/formularioRegistrarUsuario");
		}
		else
		{
		    $usu = $this->Admin_model->buscarUsu($this->input->post('username'));
		    if ($usu==0)
		    {
        	    $data = array(
        		'username' => $this->input->post('username'),
        		'password' => $this->input->post('password'),
        		'nombre' => $this->input->post('nombre'),
        		'apellido' => $this->input->post('apellido'),
        		'rol' => 'Usuario',
        		'correo' => $this->input->post('correo'),
        		'direccion' => $this->input->post('direccion'),
        		'celular' => $this->input->post('celular'),
        		'estado' => 'Activo',
        		'municipio' => $this->input->post('municipio')
        		);
        		$this->Admin_model->nuevoUsuario($data);
        		$valores['usuario'] = $this->input->post('username');
        		$valores['clave'] = $this->input->post('password');
        		$this->load->view("header");		
        		$this->load->view("authR", $valores);
        		$this->load->view("footer");
		    }
		    else
		    {
		        redirect("auth/formularioRegistrarUsuario");
		    }
		}
		
	}
}