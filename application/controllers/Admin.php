<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for Admin group only
 */
class Admin extends MY_Controller {

	protected $access = "Administrador";
	
	function __construct (){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}
	public function index(){
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("footer");
	}
	public function verUsuarios(){
		if ($this->session->userdata('rol')=='Administrador') {
			$data['registros'] = $this->Admin_model->obtenerUsuarios();	
			$this->load->view("header");
			$this->load->view("navbaradmin");
			$this->load->view("usuarios", $data);
			$this->load->view("footer");
		}
		else{
			redirect("auth");
		}
	}

	/*public function formularioRegistrarUsuario(){	
		$this->load->view("header");
		$this->load->view("navbar");
		$this->load->view("addformuser");
		$this->load->view("footer");
	}
	public function agregarUsuario(){
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'name' => $this->input->post('name'),
			'mail' => $this->input->post('mail'),
			'cellphone' => $this->input->post('cellphone'),
			'status' => $this->input->post('status'),
			'saldo' => $this->input->post('saldo')
		);
		$this->Admin_model->nuevoUsuario($data);	
		redirect('admin/verUsuarios');
				//cargamos la libreria email de ci
		$this->load->library("email");
 
		//configuracion para gmail
		$configGmail = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'caviva90@gmail.com',
			'smtp_pass' => '*953400camilo*',
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
		);    
 
		//cargamos la configuración para enviar con gmail
		$this->email->initialize($configGmail);
 
		$this->email->from('nombre o correo que envia');
		$this->email->to("caviva@hotmail.com");
		$this->email->subject('Bienvenido/a a Contaper');
		$this->email->message('<h2>Hola, ahora eres usuario de nuestro sistema</h2><hr><br>');
		$this->email->send();
		//con esto podemos ver el resultado
		var_dump($this->email->print_debugger());
	}*/
	public function formularioEditarUsuario(){
		$data['usuario'] = $this->Admin_model->obtenerUsuario($this->input->post('id_usu'));
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("editformusu", $data);
		$this->load->view("footer");
	}
	public function formularioEditarMunicipio(){
		$data['registros'] = $this->Admin_model->obtenerMunicipio($this->input->post('id_muni'), $this->input->post('id_anio'));
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("editformmuni", $data);
		$this->load->view("footer");
	}
	public function verDatosAnio(){
		$data['registros'] = $this->Admin_model->obtenerMunicipios($this->input->post('anio'));
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("municipios", $data);
		$this->load->view("footer");
	}	
	public function verGestionMunicipios(){
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("formgestionmuni");
		$this->load->view("footer");
	}
	public function editarMunicipio(){
		$id = $this->input->post('muni_id');
		$efica = $this->input->post('eficacia');
		$efici = $this->input->post('eficiencia');
		$requi = $this->input->post('requisitos');
		$gesti = $this->input->post('gestion');

		$integral = ($efica+$efici+$requi+$gesti)/4;
		if ($integral>=80) {
			$etiqueta = "Sobresaliente";
		}
		elseif ($integral>=70 and $integral<80) {
			$etiqueta = "Satisfactorio";
		}
		elseif ($integral>=60 and $integral<70) {
			$etiqueta = "Medio";
		}
		elseif ($integral>=40 and $integral<60) {
			$etiqueta = "Bajo";
		}
		elseif ($integral>=0 and $integral<40) {
			$etiqueta = "Crítico";
		}
		else {
			$etiqueta = "-";	
		}

		$data = array(
			'eficacia' => $this->input->post('eficacia'),
			'eficiencia' => $this->input->post('eficiencia'),
			'requisitos' => $this->input->post('requisitos'),
			'gestion' => $this->input->post('gestion'),
			'integral' => $integral,
			'etiqueta' => $etiqueta
		);
		$this->Admin_model->editarMuni($id, $data);
		redirect('admin/verGestionMunicipios');
	}
	public function editarUsuario(){
		$id = $this->input->post('user_id');
		$data = array(
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'mail' => $this->input->post('mail'),
			'direccion' => $this->input->post('direccion'),
			'municipio' => $this->input->post('municipio'),
			'cellphone' => $this->input->post('cellphone'),
			'estado' => $this->input->post('estado'),
			'saldo' => $this->input->post('saldo')
		);
		$this->Admin_model->editarUsu($id, $data);
		redirect('admin/verUsuarios');
	}
	public function cambiarEstadoUsuario(){
		$id = $this->input->post('user_id');
		$this->Admin_model->cambiarEstadoUsuario($id);
		redirect('admin/verUsuarios');
	}
}