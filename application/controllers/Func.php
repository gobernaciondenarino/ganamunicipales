<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for Admin group only
 */
class Func extends MY_Controller {

	protected $access = "Funcionario";
	
	function __construct (){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}
	public function index(){
		$this->load->view("header");
		$this->load->view("navbarfunc");
		$this->load->view("footer");
	}
	public function verProyectos(){
		$data['registros'] = $this->Admin_model->obtenerProyectos();	
		$this->load->view("header");
		$this->load->view("navbarfunc");
		$this->load->view("proyectos", $data);
		$this->load->view("footer");
	}
	public function formularioRegistrarProyecto(){	
		$this->load->view("header");
		$this->load->view("navbarfunc");
		$this->load->view("addformproy");
		$this->load->view("footer");
	}
	public function agregarProyecto(){
		$this->load->helper('string');
		$verify = random_string('alnum',5);		
		$config['upload_path']          = './imgproy/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 1000;
        $config['max_width']            = 10024;
        $config['max_height']           = 7068;
        $config['file_name']  = 'proyecto-'.$verify.'.png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('adjunto'))
        {                
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            $error = array('error' => $this->upload->display_errors());
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());           
        }

		if ($this->input->post('municipio')=="All") {
			$data = array(
				'nombre' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'valor' => $this->input->post('valor'),
				'municipio' => $this->input->post('municipio'),
				'autor' => $this->input->post('autor'),
				'tiempo' => $this->input->post('tiempo'),
				'estado' => $this->input->post('estado'),
				'fechaini' => $this->input->post('fechaini'),
				'fechafin' => $this->input->post('fechafin'),
				'votos' => 0,
				'resultado' => $this->input->post('resultado'),
				//'adjunto' => base_url()."imgproy/".$config['file_name']
				'adjunto' => "http://192.168.88.254/imgproy/".$config['file_name']
			);
			$this->Admin_model->nuevoProyectoT($data);	
			redirect('func/verProyectos');		
		}
		else {
			$data = array(
				'nombre' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'valor' => $this->input->post('valor'),
				'municipio' => $this->input->post('municipio'),
				'autor' => $this->input->post('autor'),
				'tiempo' => $this->input->post('tiempo'),
				'fechaini' => $this->input->post('fechaini'),
				'estado' => $this->input->post('estado'),
				'fechafin' => $this->input->post('fechafin'),
				'votos' => 0,
				'resultado' => $this->input->post('resultado'),
				'adjunto' => base_url()."imgproy/".$config['file_name']
			);
			$this->Admin_model->nuevoProyecto($data);	
			redirect('func/verProyectos');		
		}
	}
	public function formularioEditarProyecto(){
		$data['proyecto'] = $this->Admin_model->obtenerProyecto($this->input->post('id_proy'));
		$this->load->view("header");
		$this->load->view("navbarfunc");
		$this->load->view("editformproy", $data);
		$this->load->view("footer");
	}
	public function editarUsuario(){
		$id = $this->input->post('user_id');
		$data = array(
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'mail' => $this->input->post('mail'),
			'direccion' => $this->input->post('direccion'),
			'municipio' => $this->input->post('municipio'),
			'cellphone' => $this->input->post('cellphone'),
			'estado' => $this->input->post('estado')
		);
		$this->Admin_model->editarUsu($id, $data);
		redirect('admin/verUsuarios');
	}
	public function editarProyecto(){
		$id = $this->input->post('proy_id');
		$data = array(
			'nombre' => $this->input->post('nombre'),
			'descripcion' => $this->input->post('descripcion'),
			'valor' => $this->input->post('valor'),
			'municipio' => $this->input->post('municipio'),
			'autor' => $this->input->post('autor'),
			'mail' => $this->input->post('mail'),
			'tiempo' => $this->input->post('tiempo'),
			'fechaini' => $this->input->post('fechaini'),
			'fechafin' => $this->input->post('fechafin'),
			'votos' => $this->input->post('votos'),
			'resultado' => $this->input->post('resultado'),
			'estado' => $this->input->post('estado'),
			'adjunto' => $this->input->post('adjunto'),
		);
		$this->Admin_model->editarProy($id, $data);
		redirect('func/verProyectos');
	}
	public function cambiarEstadoUsuario(){
		$id = $this->input->post('user_id');
		$this->Admin_model->cambiarEstadoUsuario($id);
		redirect('admin/verUsuarios');
	}
	public function formularioAsignarSaldos(){	
		$this->load->view("header");
		$this->load->view("navbarfunc");
		$this->load->view("addformsaldo");
		$this->load->view("footer");
	}
	public function asignarSaldo(){	
		$this->Admin_model->asignarSaldoUsuarios($this->input->post('valor'), $this->input->post('municipio'));
		redirect('func/verProyectos');
	}
	public function perfil(){
		$data['registros'] = $this->Admin_model->obtenerUsuario($this->input->post('id_usu'));	
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("formperfilf", $data);
		$this->load->view("footer");
	}
	public function editarUsuarioPerfil(){
		$id = $this->input->post('user_id');
		$data = array(
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'mail' => $this->input->post('mail'),
			'direccion' => $this->input->post('direccion'),
			'municipio' => $this->input->post('municipio'),
			'cellphone' => $this->input->post('cellphone'),
			'estado' => $this->input->post('estado'),
			'saldo' => $this->input->post('saldo')
		);
		$this->Admin_model->editarUsu($id, $data);
		redirect('func/index');
	}
}