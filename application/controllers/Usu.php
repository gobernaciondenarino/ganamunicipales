<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for Admin group only
 */
class Usu extends MY_Controller {

	protected $access = "Usuario, Done";
	
	function __construct (){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}
	public function index(){
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("footer");
	}
	public function tproyectos(){
		$data['registros'] = $this->Admin_model->obtenerProyectos();	
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("tproyectos", $data);
		$this->load->view("footer");
	}
	public function mproyectos(){
		$this->Admin_model->tarjetonUsuario($this->session->userdata('id'), $this->session->userdata('municipio'));
		$data['registros'] = $this->Admin_model->obtenerTarjeton($this->session->userdata('id'));	
		$data['saldousuario'] = $this->Admin_model->obtenerUsuario($this->session->userdata('id'));
		$data['numproyectos'] = $this->Admin_model->obtenerNumProyectos($this->session->userdata('id'));
		$data['totproyectos'] = $this->Admin_model->obtenerTotProyectos($this->session->userdata('id'));
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("mproyectos", $data);
		$this->load->view("footer");
	}
	public function done(){
	    $this->Admin_model->tarjetonUsuario($this->session->userdata('id'), $this->session->userdata('municipio'));
		$data['registros'] = $this->Admin_model->obtenerTarjeton($this->session->userdata('id'));	
		$data['saldousuario'] = $this->Admin_model->obtenerUsuario($this->session->userdata('id'));
		$this->load->view("header");
		$this->load->view("navbardone");
		$this->load->view("vdone");
		$this->load->view("footer");
	}
	public function agregarVoto(){
		$this->Admin_model->realizarVoto($this->input->post('id_proy'), $this->input->post('id_user'));
		$this->Admin_model->restarSaldo($this->input->post('id_user'), $this->input->post('monto'));
		redirect('usu/mproyectos');
	}
	public function encuesta(){
		$this->Admin_model->encuestar($this->input->post('respuesta'), $this->input->post('municipio'));
		redirect('auth/logout');
	}
	public function quitarVoto(){
		$this->Admin_model->eliminarVoto($this->input->post('id_proy'), $this->input->post('id_user'));
		$this->Admin_model->sumarSaldo($this->input->post('id_user'), $this->input->post('monto'));
		redirect('usu/mproyectos');
	}
	public function resultados(){
		$data['registros'] = $this->Admin_model->obtenerProyectosC($this->session->userdata('municipio'));	
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("resproyectos", $data);
		$this->load->view("footer");
	}
	public function verProyectoUsu(){
		$data['proyecto'] = $this->Admin_model->obtenerProyecto($this->input->post('id_proy'));
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("detalleproyecto", $data);
		$this->load->view("footer");
	}
	public function vermiProyectoUsu(){
		$data['proyecto'] = $this->Admin_model->obtenerProyecto($this->input->post('id_proy'));
		$data['saldousuario'] = $this->Admin_model->obtenerUsuario($this->session->userdata('id'));
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("detallemiproyecto", $data);
		$this->load->view("footer");
	}	
	public function perfil(){
		$data['registros'] = $this->Admin_model->obtenerUsuario($this->input->post('id_usu'));	
		$this->load->view("header");
		$this->load->view("navbarusu");
		$this->load->view("formperfil", $data);
		$this->load->view("footer");
	}
	public function editarUsuario(){
		$id = $this->input->post('user_id');
		$data = array(
			'name' => $this->input->post('name'),
			'lastname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'role' => $this->input->post('role'),
			'mail' => $this->input->post('mail'),
			'direccion' => $this->input->post('direccion'),
			'municipio' => $this->input->post('municipio'),
			'cellphone' => $this->input->post('cellphone'),
			'estado' => $this->input->post('estado'),
			'saldo' => $this->input->post('saldo')
		);
		$this->Admin_model->editarUsu($id, $data);
		redirect('usu/index');
	}
	public function usudone(){
		$id = $this->input->post('id_usu');
		$this->Admin_model->editarUsuDone($id);
		$data['saldousuario'] = $this->Admin_model->obtenerUsuario($this->session->userdata('id'));
		$this->load->view("header");
		$this->load->view("navbarusu", $data);
		$this->load->view("vdone2");
		$this->load->view("footer");
	}
}