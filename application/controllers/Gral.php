<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for (all) non logged in users
 */
class Gral extends MY_Controller {	

	function __construct (){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}
	public function index()
	{	
		$this->load->view("header");	
		$this->load->view("navbargral");
		$this->load->view("footer");
	}
	public function verZonas(){
		$this->load->view("header");
		$this->load->view("navbargral");
		$this->load->view("formSubRegiones");
		$this->load->view("footer");
	}
	public function verMapaSubregion(){
		$data['registros'] = $this->Admin_model->obtenerMunicipios($this->input->post('anio'));
		$this->load->view("header");
		$this->load->view("navbargral");
		if ($this->input->post('subregion')=="Sanquianga") {
			$this->load->view("s1", $data);	
		}
		elseif ($this->input->post('subregion')=="Pacifico Sur") {
			$this->load->view("s2", $data);	
		}
		elseif ($this->input->post('subregion')=="Telembí") {
			$this->load->view("s3", $data);	
		}
		elseif ($this->input->post('subregion')=="Pie de Monte Costero") {
			$this->load->view("s4", $data);	
		}
		elseif ($this->input->post('subregion')=="Exprovincia de Obando") {
			$this->load->view("s5", $data);	
		}
		elseif ($this->input->post('subregion')=="Sabana") {
			$this->load->view("s6", $data);	
		}
		elseif ($this->input->post('subregion')=="Abades") {
			$this->load->view("s7", $data);	
		}
		elseif ($this->input->post('subregion')=="Occidente") {
			$this->load->view("s8", $data);	
		}
		elseif ($this->input->post('subregion')=="Cordillera") {
			$this->load->view("s9", $data);	
		}
		elseif ($this->input->post('subregion')=="Centro") {
			$this->load->view("s10", $data);	
		}
		elseif ($this->input->post('subregion')=="Juanambú") {
			$this->load->view("s11", $data);	
		}
		elseif ($this->input->post('subregion')=="Rio Mayo") {
			$this->load->view("s12", $data);	
		}
		elseif ($this->input->post('subregion')=="Guambuyaco") {
			$this->load->view("s13", $data);	
		}
		$this->load->view("footer");
	}
	public function verDatosMunicipio(){
		$data['registros'] = $this->Admin_model->obtenerMunicipio($this->input->post('id_muni'), $this->input->post('id_anio'));
		$this->load->view("header");
		$this->load->view("navbaradmin");
		$this->load->view("municipios", $data);
		$this->load->view("footer");
	}
}