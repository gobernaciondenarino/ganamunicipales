<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This controller can be accessed 
 * for all logged in users
 */
class Dashboard extends MY_Controller {	

	function __construct (){
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}
	public function index()
	{
		if ($this->session->userdata('rol')=='Administrador') {
			$this->load->view("header");
			$this->load->view("navbaradmin");
			$this->load->view("footer");
		}
		if ($this->session->userdata('rol')=='Funcionario') {
			$this->load->view("header");
			$this->load->view("navbarfunc");
			$this->load->view("footer");
		}
		if ($this->session->userdata('rol')=='Usuario') {
			redirect('usu/mproyectos');
		}
		if ($this->session->userdata('rol')=='Done') {
		    redirect('usu/done');
		}
	}

}